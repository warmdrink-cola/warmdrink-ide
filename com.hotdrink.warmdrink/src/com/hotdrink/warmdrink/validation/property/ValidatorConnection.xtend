package com.hotdrink.warmdrink.validation.property

import com.hotdrink.warmdrink.warmLang.RegularRelation
import java.util.List

class ValidatorConnection {
	RegularRelation rel;
	List<String> args;

	new(RegularRelation rel, List<String> args) {
		this.rel = rel;
		this.args = args;
	}

	override toString() {
		val builder = new StringBuilder();
		builder.append(rel.name + " ");
		for (String s : args) {
			builder.append(s + " ");
		}
		return builder.toString;
	}

	def getRel() {
		return rel;
	}

	def getArgs() {
		return args;
	}
	
	override equals(Object o) {
		switch o {
			ValidatorConnection: {
				rel === o.rel && args.equals(o.args)
			}
			default: false
		}
	}
	
	override hashCode() {
		return 1;
		// rel.hashCode * args.hashCode
	}
}
