package com.hotdrink.warmdrink.validation

import com.hotdrink.warmdrink.warmLang.StructureType

class StructureTypeUtil {
	def public static String typeName(StructureType type) {
		type.structure.name + (type.isList ? "*" : "")
	}
	
	def public static boolean matchType(StructureType a, StructureType b) {
		a.structure.equals(b.structure) && a.isList === b.isList
	}
}