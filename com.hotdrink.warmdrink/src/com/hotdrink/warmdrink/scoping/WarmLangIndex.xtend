package com.hotdrink.warmdrink.scoping

import org.eclipse.xtext.mwe.ResourceDescriptionsProvider
import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import com.hotdrink.warmdrink.warmLang.WarmLangPackage

class WarmLangIndex {
	@Inject ResourceDescriptionsProvider rdp
	
	def getResourceDescription(EObject o) {
		val index = rdp.get(o.eResource.resourceSet)
		index.getResourceDescription(o.eResource.URI)
	}
	
	def getExportedEObjectDescriptions(EObject o) {
		o.getResourceDescription.getExportedObjects
	}
	
	def getExportedClassesEObjectDescriptions(EObject o) {
		o.getResourceDescription.getExportedObjectsByType(WarmLangPackage.eINSTANCE.model)
	}
}
