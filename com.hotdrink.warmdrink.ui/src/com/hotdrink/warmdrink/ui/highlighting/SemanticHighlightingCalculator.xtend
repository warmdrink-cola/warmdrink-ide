package com.hotdrink.warmdrink.ui.highlighting

import com.hotdrink.warmdrink.warmLang.GenericConnection
import com.hotdrink.warmdrink.warmLang.RegularRelation
import com.hotdrink.warmdrink.warmLang.WarmLangPackage
import org.eclipse.xtext.CrossReference
import org.eclipse.xtext.ide.editor.syntaxcoloring.IHighlightedPositionAcceptor
import org.eclipse.xtext.ide.editor.syntaxcoloring.ISemanticHighlightingCalculator
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.eclipse.xtext.resource.XtextResource
import org.eclipse.xtext.util.CancelIndicator
import com.hotdrink.warmdrink.warmLang.AbstractArgument
import com.hotdrink.warmdrink.warmLang.InstantiatedRule
import com.hotdrink.warmdrink.warmLang.RelationRef

class SemanticHighlightingCalculator implements ISemanticHighlightingCalculator {

	override provideHighlightingFor(XtextResource resource, IHighlightedPositionAcceptor acceptor,
		CancelIndicator cancelIndicator) {
		if(resource === null || resource.getParseResult() === null) return;
		var INode root = resource.getParseResult().getRootNode()
		for (INode node : root.getAsTreeIterable()) {
			val elem = NodeModelUtils.findActualSemanticObjectFor(node)
			switch (elem) {
				AbstractArgument:
					acceptor.addPosition(
						node.getOffset(),
						node.getLength(),
						WarmLangHighlightingConfiguration.AT
					)
				GenericConnection: {
					val grammarElement = node.grammarElement
					switch (grammarElement) {
						CrossReference:
							if (grammarElement.type.classifier.name == "GenericPlaceholder")
								acceptor.addPosition(node.getOffset(), node.getLength(),
									WarmLangHighlightingConfiguration.AT)
					}
				}
				RegularRelation: {
					if (NodeModelUtils.findNodesForFeature(
						elem,
						WarmLangPackage.Literals.REGULAR_RELATION__NAME
					).map[n|n.textRegion].contains(node.textRegion)) {
						acceptor.addPosition(node.getOffset(), node.getLength(),
							WarmLangHighlightingConfiguration.RELATION)
					}
				}
				RelationRef: {
					val grammarElement = node.grammarElement
					switch (grammarElement) {
						CrossReference: {
							acceptor.addPosition(node.getOffset(), node.getLength(),
							WarmLangHighlightingConfiguration.RELATION)
						}
					}
				}
			}
		}
	}
}
