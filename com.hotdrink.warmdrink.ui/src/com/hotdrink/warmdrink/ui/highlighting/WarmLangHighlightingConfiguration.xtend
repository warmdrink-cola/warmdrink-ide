package com.hotdrink.warmdrink.ui.highlighting

import org.eclipse.swt.SWT
import org.eclipse.swt.graphics.RGB
import org.eclipse.xtext.ui.editor.syntaxcoloring.DefaultHighlightingConfiguration
import org.eclipse.xtext.ui.editor.syntaxcoloring.IHighlightingConfigurationAcceptor
import org.eclipse.xtext.ui.editor.utils.TextStyle

class WarmLangHighlightingConfiguration extends DefaultHighlightingConfiguration {
	
	public static final String AT = "@"
	public static final String RELATION = "REL"
	
	override configure(IHighlightingConfigurationAcceptor acceptor) {
		super.configure(acceptor);
		acceptor.acceptDefaultHighlighting(AT, "abstract", abstractTextStyle())
		acceptor.acceptDefaultHighlighting(RELATION, "relation", relationTextStyle())
	}
	
	def private TextStyle abstractTextStyle() {
		val t = defaultTextStyle().copy()
		t.setColor(new RGB(224, 216, 86));
		t.setStyle(SWT.ITALIC);
		return t
	}
	
	def private TextStyle relationTextStyle() {
		val t = defaultTextStyle().copy()
		t.setColor(new RGB(26, 71, 193));
		// t.setStyle(SWT.BOLD);
		return t
	}
	
}