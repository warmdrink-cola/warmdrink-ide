package com.hotdrink.warmdrink.validation.property

import java.util.ArrayList
import java.util.HashSet
import java.util.List
import java.util.Set

class Permutations {

	def static List<List<String>> getPermutations(List<String> list, int n) {
		if (n > list.size) return new ArrayList()
		val set = new HashSet()
		set.addAll(list);
		return getPermutationsHelper(set, n)
	}
	
	def static private List<List<String>> getPermutationsHelper(Set<String> set, int n) {
		val result = new ArrayList<List<String>>()
		if (n==0) {
			result.add(new ArrayList<String>())
			return result
		} else {
			for (String s : set.immutableCopy) {
				set.remove(s);
				for (List<String> l : getPermutationsHelper(set, n-1)) {
					l.add(0, s);
					result.add(l);
				}
				set.add(s)
			}
			return result
		}
	}
	
	def static private String showPermutations(List<List<String>> ls) {
		val s = new StringBuilder()
		for(List<String> l : ls) {
			for (String p : l){
				s.append(p + " ")
			}
			s.append(", ")
		}
		return s.toString
	}	
}