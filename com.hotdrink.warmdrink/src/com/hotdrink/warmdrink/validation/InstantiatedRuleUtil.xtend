package com.hotdrink.warmdrink.validation

import com.hotdrink.warmdrink.warmLang.GenericConnection
import com.hotdrink.warmdrink.warmLang.GenericMultiCaseRule
import com.hotdrink.warmdrink.warmLang.GenericRelation
import com.hotdrink.warmdrink.warmLang.GenericRule
import com.hotdrink.warmdrink.warmLang.GenericSingleCaseRule
import com.hotdrink.warmdrink.warmLang.InstantiatedRule
import com.hotdrink.warmdrink.warmLang.RegularRelation
import com.hotdrink.warmdrink.warmLang.StructureType
import java.util.ArrayList
import java.util.HashMap
import java.util.List
import java.util.Map

import static extension com.hotdrink.warmdrink.validation.StructureTypeUtil.*
import static extension org.eclipse.xtext.EcoreUtil2.*
import com.hotdrink.warmdrink.warmLang.TypedArgument
import com.hotdrink.warmdrink.warmLang.AbstractArgument

class InstantiatedRuleUtil {

	enum MessageType {
		WARNING,
		ERROR
	}

	/**
	 * Infers types of abstract arguments and reports error if same argument has several different types
	 */
	def static Pair<MessageType, String> inferArgumentTypesOfGenericRule(InstantiatedRule rule) {
		val genericRule = rule.genericRule
		
		// map abstract relations in generic rule to actual relations in instantiated rule
		val Map<GenericRelation, RegularRelation> relMap = new HashMap
		for (var i = 0; i < rule.relationParameters.size; i++) {
			relMap.put(genericRule.rels.rels.get(i), rule.relationParameters.get(i).rel)
		}

		// infer types of abstract arguments and report error if same argument is found twice with different types
		val Map<AbstractArgument, StructureType> types = new HashMap
		for (GenericConnection conn : genericRule.allConnections) {
			for (var i = 0; i < conn.args.size; i++) {
				val arg = conn.args.get(i)
				val expectedType = relMap.get(conn.rel).args.get(i).getContainerOfType(TypedArgument).type
				if (types.containsKey(arg) && !types.get(arg).matchType(expectedType)) {
					return new Pair(MessageType.ERROR,
						"Cannot unify type " + expectedType.typeName + " with type " + types.get(arg).typeName +
							" of abstract placeholder " + arg.name + " in generic rule " + genericRule.name + ".")
				}
				types.put(arg, expectedType)
			}
		}
		return null
	}

	def private static List<GenericConnection> getAllConnections(GenericRule rule) {
		val List<GenericConnection> result = new ArrayList
		switch rule {
			GenericSingleCaseRule: {
				result.addAll(rule.preConnections)
				result.addAll(rule.postConnections)
			}
			GenericMultiCaseRule: {
				result.addAll(rule.cases.map[c | c.preConnections].flatten)
				result.addAll(rule.cases.map[c | c.postConnections].flatten)
			}
		}
		return result
	}
}
