/*
 * generated by Xtext 2.21.0
 */
package com.hotdrink.warmdrink.ide

import com.google.inject.Guice
import com.hotdrink.warmdrink.WarmLangRuntimeModule
import com.hotdrink.warmdrink.WarmLangStandaloneSetup
import org.eclipse.xtext.util.Modules2

/**
 * Initialization support for running Xtext languages as language servers.
 */
class WarmLangIdeSetup extends WarmLangStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new WarmLangRuntimeModule, new WarmLangIdeModule))
	}
	
}
