package com.hotdrink.warmdrink.validation.property

import com.hotdrink.warmdrink.warmLang.RegularRelation
import java.util.List

class ExpectConnection extends ValidatorConnection {
	boolean no; 
	
	new(RegularRelation rel, List<String> args, boolean no) {
		super(rel, args)
		this.no = no;
	}

	override toString() {
		(no ? "no " : "") + super.toString
	}
	
	def connString() {
		super.toString
	}
	
	def getNo() {
		return no;
	}	
}